# Website Sentiment Extraction

![Demo](demo.gif)

A project I built to practice coding in go and learn about natural language processing.

## Features:

- Caching
- Web Interface
- Some Colors

## Resources:

### Text Extraction

https://github.com/k3a/html2text

### Sentence Segmentation and Named Entity Extraction

https://github.com/jdkato/prose

### Sentiment Analysis

https://github.com/jonreiter/govader

## How it works

1. Feed in a URL
2. Get html data
3. Extract Text
4. Create list of sentences.
5. Extract Named Entity and Sentiment for each sentence
6. Record average Sentiment for each Named entity and their occurrence
7. Return basic page displaying this information

## Todo:

- Add timeout on long requests
