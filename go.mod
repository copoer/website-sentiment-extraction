module ooper.space/go-sentiment

go 1.16

require (
	github.com/copoer/html2text v0.0.0-20210303000848-0ca1ae7376d4
	github.com/jdkato/prose/v2 v2.0.0
	github.com/jonreiter/govader v0.0.0-20210224072402-ab79f4c25a36
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/ssor/bom v0.0.0-20170718123548-6386211fdfcf // indirect
	golang.org/x/net v0.0.0-20210326060303-6b1517762897 // indirect
)
