package main

import (
	"github.com/copoer/html2text"
	"github.com/jdkato/prose/v2"
	"github.com/jonreiter/govader"
	"github.com/patrickmn/go-cache"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"
)

var entityCache = cache.New(60*time.Minute, 10*time.Minute)

type Entity struct {
	Count     int
	Sentiment float64
	Name      string
}

func (a Entity) AvgSentiment() float64 { return a.Sentiment / float64(a.Count) }

func (a Entity) SentimentColor() string {
	sent := a.AvgSentiment()
	if sent >= 0 {
		return "green"
	} else {
		return "red"
	}
}

type NES struct {
	Count     int
	NE        string
	Sentiment float64
}

type EntityArray []*Entity

func (a EntityArray) Len() int           { return len(a) }
func (a EntityArray) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a EntityArray) Less(i, j int) bool { return a[i].Count > a[j].Count }

func getText(urlStr string) (plain string) {
	resp, err := http.Get(urlStr)
	if err != nil {
		print(err)
	}
	defer resp.Body.Close()
	text, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err)
	}
	plain, _ = html2text.FromString(string(text), html2text.Options{JustText: true, OmitLinks: true})
	return
}

func extractNES(plain string) (c []*Entity) {
	doc, _ := prose.NewDocument(plain)
	model := doc.Model
	sents := doc.Sentences()
	analyzer := govader.NewSentimentIntensityAnalyzer()
	var list = make(map[string]*Entity)
	// Extract NE and sentiment
	for _, sent := range sents {
		sentd, _ := prose.NewDocument(sent.Text, prose.WithSegmentation(false), prose.UsingModel(model))
		sentiment := analyzer.PolarityScores(sent.Text)

		for _, ent := range sentd.Entities() {
			var str = strings.TrimSpace(ent.Text)
			_, exists := list[str]
			if exists {
				list[str].Count++
				list[str].Sentiment += sentiment.Compound
			} else {
				var values = &Entity{1, sentiment.Compound, str}
				list[str] = values
			}
		}
	}
	// Sort
	c = []*Entity{}
	for _, value := range list {
		c = append(c, value)
	}
	sort.Sort(EntityArray(c))
	return
}

func fillTemplate(w http.ResponseWriter, data []*Entity, url string) {
	const tpl = `
<!DOCTYPE html>
	<head>
		<meta charset="UTF-8">
		<title>Page Sentiments</title>
	</head>
	<style>
	table, th, td {
		border-collapse: collapse;
		padding: 0.5rem;
		border-bottom: solid;
		max-width: 25rem;
	}
	</style>
	<body>
	<center>
	<h1>{{ .Title }}</h1>
	<table>
	<tr>
		<th> Named Entity </th>
		<th> Average Sentiment </th>
		<th> Occurrences </th>
	</tr>
	{{range .Items}}
	<tr>
		<td>{{ .Name }}</td>
		<td><span style="color:{{.SentimentColor}}">{{ .AvgSentiment }}</span></td>
		<td>{{ .Count }}</td>
	</tr>
	{{else}}Error{{end}}
	</table>
	</center>
	</body>
</html>
	`
	t, err := template.New("template").Parse(tpl)
	if err != nil {
		log.Fatal(err)
	}
	input := struct {
		Title string
		Items []*Entity
	}{
		Title: url,
		Items: data,
	}

	err = t.Execute(w, input)
	if err != nil {
		log.Fatal(err)
	}
}

func sentimentHandler(w http.ResponseWriter, r *http.Request) {
	urlObj := r.URL.Query()["url"]
	_, err := url.ParseRequestURI(urlObj[0])
	urlStr := urlObj[0]
	if err == nil {
		// Attempt to retrieve from cache
		output, found := entityCache.Get(urlStr)
		if !found {
			output := extractNES(getText(urlStr))
			entityCache.Set(urlStr, output, cache.DefaultExpiration)
			fillTemplate(w, output, urlStr)
		} else {
			fillTemplate(w, output.([]*Entity), urlStr)
		}
	} else {
		io.WriteString(w, "{'Error':'Invalid URL'}")
	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	const tpl = `
	<!DOCTYPE html>
	<head>
		<meta charset="UTF-8">
		<title>Page Sentiments</title>
	</head>
	<style>
	.container {
		display: flex;
		justify-content: space-around;
		padding-top: 10rem;
	}
	.button {
		background: white;
		padding: 0.5rem;
	}
	</style>
	<body>
	<div class="container">
	<div>
	<h1>Website Sentiment Analysis</h1>
	<form action="/sentiments">
	<label for="url">Enter URL</label>
	<input type="text" id="url" name="url">
	<input class="button" type="submit" value="Submit">
	</form>
	</div>
	</div>
	</body>
</html>
	`
	io.WriteString(w, tpl)
}

func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/sentiments", sentimentHandler)
	s := &http.Server{
		Addr:         ":8080",
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}
	if err := s.ListenAndServe(); err != nil {
		log.Fatal("An Error Occurred: %s", err)
	}
}
